# InstaBot

This instabot program is designed for secure use, ensuring that no one can gain access to your account. It is also continuously being improved with new features to enhance the user experience.

I have an instabot program that is a script and is suitable for programmers. It is specifically designed for secure use, ensuring that no one can gain access to your account.

The current function of this program is to detect those who have unfollowed you and are not following you back, but additional options will be added later.

## Features

1. **Unfollower Detection:** InstaBot has the capability to find users who have unfollowed you on Instagram.
2. **Non-Following Back:** It can also display the list of users whom you follow but do not follow you back.

* another features will be added soon .

## Installation and Setup

Follow the steps below to install and set up InstaBot:

1. Step 1: Clone the repository using the command `gitclone + [repository URL]`.
2. Step 2: Navigate to the cloned directory using the command `cd insta-bot`.

## Usage

To use InstaBot, follow these steps:

1. Run the command `python main.py`.
2. Enter your username and password.
3. If everything goes well, the program will log in to your Instagram account easily.

**Notes**:

1. Upon the first run, the program will load the initial account information, and usually, it will only display the list of users whom you follow but do not follow you back.

2. After the first successful login to your account, you do not need to enter your username and password again. Just select the "login" option.


## Contribution and License

InstaBot does not currently accept contributions, and there is no specified license for this project.

## Credits

This program utilizes the `instaloader` library for its design and functionality.
