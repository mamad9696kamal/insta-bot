from utils.datastore.storage_manager import FileStorage, MongoStorage


class StoreClass:
    @staticmethod
    def set_store(store_type):
        if store_type == "file":
            return FileStorage
        else:
            return MongoStorage

    @staticmethod
    def set_loder(load_type):
        if load_type == "file":
            return FileStorage
        else:
            return MongoStorage

    @staticmethod
    def apply_new_data(type_of_store, data):
        store = StoreClass.set_store(type_of_store)
        store.save(data)
        print("New data replaced in data store.")
