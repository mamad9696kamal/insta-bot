from utils.datastore import StoreClass
from utils.mixins import InstaCheckClassMixin
from colorama import Fore


class InitializerMixin:
    insta_check_methods = InstaCheckClassMixin
    __slots__ = ["_user", "check_methods"]

    def __init__(self, user):
        self._user = user
        self.check_methods = self.insta_check_methods.get_user(self._user)

    @classmethod
    def get_user(cls, username):
        return cls(username)

    def initial_site_data(self, store_type="file", store: bool = False) -> dict:
        """
        Show all information about follower|followings count, name and not follow back people.

        :param store: data will be saved if True, else: it's just will be  shown.
        :param store_type: select a type of file to save data.
        :return: dict.
        """
        data = dict(
            user=self._user,
            followers_count=self.check_methods.followers_count,
            following_count=self.check_methods.followings_count,
            followers_name=self.check_methods.followers_by_name,
            followins_name=self.check_methods.followings_by_name,
            not_following_back=self.check_methods.none_followers,
            mutual_followers=self.check_methods.mutual_followers,
        )

        if store:
            store = StoreClass().set_store(store_type)
            store.save(data)
            print(Fore.GREEN + "\ndata successfully saved!..")
        return data
