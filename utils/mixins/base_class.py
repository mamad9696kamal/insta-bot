from abc import ABC
import instaloader
from colorama import Fore


class BaseClassMixin(ABC):
    """
    A mixin class only for inheritance. contains internal methods to
    simplify the syntax in the inherited classes.
    """

    loder = instaloader.Instaloader()

    @classmethod
    def login(cls, user, passw):
        print(Fore.LIGHTWHITE_EX + "logining ..")
        cls.loder.login(user, passw)
        print(Fore.GREEN + "\tlogged in!\n")

    @classmethod
    def profile(cls, username):
        return instaloader.Profile.from_username(cls.loder.context, username)
