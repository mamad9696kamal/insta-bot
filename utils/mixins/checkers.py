import sys
from utils.mixins import BaseClassMixin


class InstaCheckClassMixin:
    """
    Contain check methods such as:
        both, followers_count, followings_count, not back followers_by_name, mutual followers_by_name.
    """

    __slots__ = ["__user", "__profile", "followers_by_name", "followings_by_name"]
    base = BaseClassMixin

    def __init__(self, user):
        self.__user = user
        self.__profile = self.base.profile(username=self.__user)
        self.followers_by_name = self._check_followers_name()
        self.followings_by_name = self._check_followings_name()

    @classmethod
    def get_user(cls, username):
        return cls(username)

    def _check_followers_name(self) -> list:
        try:
            followers = self.__profile.get_followers()
        except Exception:
            print("*** login required ***")
            sys.exit()

        names = [follower.username for follower in followers]

        return names

    def _check_followings_name(self) -> list:
        try:
            followings = self.__profile.get_followees()
        except Exception:
            print("*** login required ***")
            sys.exit()

        names = [following.username for following in followings]

        return names

    @property
    def followers_count(self):
        return self.__profile.followers

    @property
    def followings_count(self):
        return self.__profile.followees

    @property
    def none_followers(self) -> list:
        """
        check the follow back in your account.

        :return: list of followings.
        """

        not_following_back: list = list(
            set(self.followings_by_name) - set(self.followers_by_name)
        )

        return not_following_back

    @property
    def mutual_followers(self) -> list:
        """
        check the follow back in your account.

        :return: list of mutual followers.
        """

        mutual_followers: list = list(
            set(self.followers_by_name).intersection(self.followings_by_name)
        )

        return mutual_followers
