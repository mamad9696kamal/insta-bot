import os
from configs import BASE_DIR


def register():
    os.chdir(BASE_DIR)
    try:
        os.mkdir("data")
        return True
    except FileExistsError:
        return False
