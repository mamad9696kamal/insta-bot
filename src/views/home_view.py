import time
from colorama import Fore
import sys
from src import banner
from src.views import not_back
from src.views import unfollow

# from utils.mixins.initializer import StoreClass
from utils.instagram import Instagram


class HomeView:
    def home(self, site_data):
        banner()
        time.sleep(0.5)
        print(
            Fore.GREEN
            + "\t-[created  bye %-SIRIUS-% ] - instagram :_awmirsn_\n\n"
            + Fore.LIGHTCYAN_EX
            + "{1}   -who does not back to you-\n\n"
            + "{2}   -who does unfollow you recently-\n\n"
            + Fore.LIGHTWHITE_EX
            + "{00}   -Apply new data and Exit-\n\n"
        )
        try:
            chose = input(
                Fore.LIGHTRED_EX
                + "\u2501["
                + Fore.GREEN
                + " InstaBot "
                + Fore.LIGHTRED_EX
                + "]\u2501"
                + Fore.LIGHTWHITE_EX
                + R" \ "
                + Fore.YELLOW
                + " Home"
                + Fore.LIGHTWHITE_EX
                + R" \  "
                + Fore.LIGHTRED_EX
                + "\u2501 >"
                + Fore.LIGHTCYAN_EX
                + "\n$"
                + Fore.LIGHTBLUE_EX
                + " input : "
            )
        except KeyboardInterrupt:
            sys.exit()

        if chose == "1":
            not_back(data=site_data)
            self.home(site_data)
        elif chose == "2":
            unfollow(data=site_data)
            self.home(site_data)
        elif chose == "00":
            Instagram.store.apply_new_data(type_of_store="file", data=site_data)
            print()
            sys.exit()
        else:
            print(
                Fore.RED
                + "Value error! you have to enter a number between 1-4,Try again!...\n\n"
            )
            self.home(site_data)
