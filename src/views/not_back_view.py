import sys
import time
from src import banner
from utils.instagram import Instagram
from colorama import Fore


def not_back(data):
    banner()
    time.sleep(0.5)
    Instagram.analyzer(data, "file").search_none_follow()
    try:
        input(Fore.LIGHTYELLOW_EX + "\n\nPress 'Enter' back to home ..")
    except KeyboardInterrupt:
        sys.exit()
